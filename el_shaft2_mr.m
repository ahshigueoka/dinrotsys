function M_R_e = el_shaft2_mr(model, vol_idx)
% array = el_shaft2_mr(model m, int vol_idx)
%
% Creates the elementar rotational inertia matrix from the properties
% defined in the volume identified by "vol_idx" and the data given in the
% model structure "m".
%
    geo_idx = model.volumes{vol_idx}.geometry;
    mat_idx = model.volumes{vol_idx}.material;
    
    r_o = model.geometry_data{geo_idx}.r_o;
    r_i = model.geometry_data{geo_idx}.r_i;
    A = pi*(r_o^2 - r_i^2);
    
    L = model.geometry_data{geo_idx}.L / model.volumes{vol_idx}.num_int_elements;
    
    rho = model.material{mat_idx}.rho;
    mu = rho*A;
    
    M_R_e = [   36        0        0        3*L    -36        0        0        3*L  ;...
                 0       36       -3*L      0        0      -36       -3*L      0    ;...
                 0       -3*L      4*L^2    0        0        3*L       -L^2    0    ;...
                 3*L      0        0        4*L^2   -3*L      0        0         -L^2;...
               -36        0        0       -3*L     36        0        0      -3*L  ;...
                 0      -36        3*L      0        0       36        3*L      0    ;...
                 0       -3*L       -L^2    0        0        3*L      4*L^2    0    ;...
                 3*L      0        0         -L^2   -3*L      0        0        4*L^2];
   M_R_e = mu * (r_o^2 - r_i^2) / (120 * L) * M_R_e;
end
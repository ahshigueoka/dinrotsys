function [M_F, G_F, C_F, K_F, F_F] = apply_boundary_conditions(model, M_F, G_F, C_F, K_F, F_F)

    for bc_j = model.num_bcs:-1:1
        node  = model.bcs(bc_j, 1);
        DOF   = model.bcs(bc_j, 2);
        value = model.bcs(bc_j, 3);
        
        line_num = (node - 1)*model.node_DOFs + DOF;
        
        M_F(line_num, :) = [];
        M_F(:, line_num) = [];
        
        G_F(line_num, :) = [];
        G_F(:, line_num) = [];
        
        if ~isempty(C_F)
            C_F(line_num, :) = [];
            C_F(:, line_num) = [];
        end
        
        F_F = F_F - K_F(:, line_num)*value;
        F_F(line_num) = [];
        
        K_F(line_num, :) = [];
        K_F(:, line_num) = [];
    end
end
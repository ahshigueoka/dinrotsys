function K_T_b = el_bearing2_kt(model, bearing_idx)
    K_T_b = model.bearings{bearing_idx}.K;
end
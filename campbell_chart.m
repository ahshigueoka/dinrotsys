function campbell_chart(model, M_F, G_F, C_F, K_F, freq_vec, num_freq_r)
    num_samples = length(freq_vec);
    freq_r = zeros(num_freq_r, num_samples);
    
    for omega_j = 1:num_samples
        if isempty(C_F)
            A = sparse([zeros(size(K_F)), eye(size(K_F)); K_F, -2*pi*freq_vec(omega_j)*G_F]);
            B = sparse([-eye(size(K_F)), zeros(size(K_F)); zeros(size(K_F)), M_F]);
        else
            A = sparse([zeros(size(K_F)), eye(size(K_F)); K_F, C_F-2*pi*freq_vec(omega_j)*G_F]);
            B = sparse([-eye(size(K_F)), zeros(size(K_F)); zeros(size(K_F)), M_F]);
        end
        omega_n = eigs(A, -1i*B, 2*num_freq_r, 0);
        freq_n = sort(abs(omega_n((1:num_freq_r)*2-1)) / (2 * pi));
        freq_r(:, omega_j) = freq_n;
        
        clear A B
    end
    
    % line_color = 1 - exp(-(0:num_omega_r)'/num_omega_r*5)*[1 1 1];
    line_color = hsv(num_freq_r)*0.7;
    
    fig_h = figure('OuterPosition', [0, 50, 500, 500]);
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 16]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 16]);
    ax_h = axes;
    
    hold on
    plot(freq_vec, freq_vec, 'LineStyle', '-', 'Color', [.6 .6 .6])
    for curve_j = 1:num_freq_r
        plot(freq_vec, freq_r(curve_j, :), 'LineStyle', '-', ...
            'Color', line_color(curve_j, :))
    end
    hold off
    
    axis equal
    axis square
    xlabel('\Omega (Hz)')
    ylabel('\omega_r (Hz)')
    title('Campbell Chart')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));

    plot_flags = [0, 1, 3];
    save_figure(fig_h, 'Campbell_chart', model.fig_dir, plot_flags);
end
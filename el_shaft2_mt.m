function M_T_e = el_shaft2_mt(model, vol_idx)
% array = el_shaft2_mt(model m, int vol_idx)
%
% Creates the elementar translational inertia matrix from the properties
% defined in the volume identified by "vol_idx" and the data given in the
% model structure "m".
%
    geo_idx = model.volumes{vol_idx}.geometry;
    mat_idx = model.volumes{vol_idx}.material;
    
    r_o = model.geometry_data{geo_idx}.r_o;
    r_i = model.geometry_data{geo_idx}.r_i;
    A = pi*(r_o^2 - r_i^2);
    
    L = model.geometry_data{geo_idx}.L / model.volumes{vol_idx}.num_int_elements;
    
    rho = model.material{mat_idx}.rho;
    mu = rho*A;
    
    M_T_e = [  156        0        0       22*L     54        0        0      -13*L  ;...
                 0      156      -22*L      0        0       54       13*L      0    ;...
                 0      -22*L      4*L^2    0        0      -13*L     -3*L^2    0    ;...
                22*L      0        0        4*L^2   13*L      0        0       -3*L^2;...
                54        0        0       13*L    156        0        0      -22*L  ;...
                 0       54      -13*L      0        0      156       22*L      0    ;...
                 0       13*L     -3*L^2    0        0       22*L      4*L^2    0    ;...
               -13*L      0        0       -3*L^2  -22*L      0        0        4*L^2];
   M_T_e = mu * L / 420 * M_T_e;
end
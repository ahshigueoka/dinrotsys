function G_F = assemble_matrix_gyroscopic(model)
    % Preallocate the global matrix
    total_num_nodes = model.mesh.num_nodes*model.node_DOFs;
    G_F = zeros(total_num_nodes, total_num_nodes);
    
    % Add the inertia terms, one volume at a time
    for vol_j = 1:model.num_volumes
        % Use the same matrix for all elements in this volume, since all
        % properties are the same.
        G_e = el_shaft2_g(model, vol_j);
        
        % Assemble inertia terms from each element internal to this volume
        int_elements = model.volumes{vol_j}.int_elements;
        for el_idx = int_elements
            n1 = model.mesh.elements(el_idx, 1);
            n2 = model.mesh.elements(el_idx, 2);

            % Each matrix has been subdivided into quadrants
            %    a b
            % a [1 2]
            % b [3 4]
            % Assemble the first quadrant
            node_offset_a = (n1-1)*model.node_DOFs;
            element_nodes_a = 1:model.node_DOFs;
            global_nodes_a = node_offset_a + (1:model.node_DOFs);
            G_F(global_nodes_a, global_nodes_a) = ...
                G_F(global_nodes_a, global_nodes_a) ...
                + G_e(element_nodes_a, element_nodes_a);
            
            % Assemble the second quadrant
            node_offset_a = (n1-1)*model.node_DOFs;
            node_offset_b = (n2-1)*model.node_DOFs;
            element_nodes_a = 1:model.node_DOFs;
            element_nodes_b = model.node_DOFs + (1:model.node_DOFs);
            global_nodes_a = node_offset_a + (1:model.node_DOFs);
            global_nodes_b = node_offset_b + (1:model.node_DOFs);
            G_F(global_nodes_a, global_nodes_b) = ...
                G_F(global_nodes_a, global_nodes_b) ...
                + G_e(element_nodes_a, element_nodes_b);
            
            % Assemble the third quadrant
            node_offset_a = (n2-1)*model.node_DOFs;
            node_offset_b = (n1-1)*model.node_DOFs;
            element_nodes_a = model.node_DOFs + (1:model.node_DOFs);
            element_nodes_b = 1:model.node_DOFs;
            global_nodes_a = node_offset_a + (1:model.node_DOFs);
            global_nodes_b = node_offset_b + (1:model.node_DOFs);
            G_F(global_nodes_a, global_nodes_b) = ...
                G_F(global_nodes_a, global_nodes_b) ...
                + G_e(element_nodes_a, element_nodes_b);
            
            % Assemble the fourth quadrant
            node_offset_b = (n2-1)*model.node_DOFs;
            element_nodes_b = model.node_DOFs + (1:model.node_DOFs);
            global_nodes_b = node_offset_b + (1:model.node_DOFs);
            G_F(global_nodes_b, global_nodes_b) = ...
                G_F(global_nodes_b, global_nodes_b) ...
                + G_e(element_nodes_b, element_nodes_b);
        end
    end
    
    % Add the inertia terms from the disks
    for disk_j = 1:model.num_disks
        n1 = model.mesh.elements_disk(disk_j);
        G_d = el_disk2_g(model, disk_j);
        
        node_offset_a = (n1-1)*model.node_DOFs;
        element_nodes_a = 1:model.node_DOFs;
        global_nodes_a = node_offset_a + (1:model.node_DOFs);
        G_F(global_nodes_a, global_nodes_a) = ...
            G_F(global_nodes_a, global_nodes_a) ...
            + G_d(element_nodes_a, element_nodes_a);
    end
end
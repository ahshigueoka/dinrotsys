function model = generate_mesh(model)
% Create a vector of uniformly spaced nodes in each volume, according to
% the following procedure:
%   - Determine the number "num_el" of uniformly spaced nodes in each
%     volume, according to given "element_size".
%   - Check if the number of elements is smaller than the minimal
%     number of elements.
%       - In case it is, force "num_el" to the minimal number of elements
%         in the volume.
%       - In case it is not, check if the maximal number of elements has
%         been exceeded.
%           - In case it has, then force "num_el" to the maximal number of
%             elements in the volume.
%   - Create a vector of linearly
%
    int_nodes_pos = cell(model.num_volumes, 1);
    
    % Index the station nodes
    station_nodes = 1:model.num_stations;
    node_counter = length(station_nodes);
    
    element_counter = 0;
    
    for vol_j = 1:model.num_volumes
        % Calculate the number of elements according to "element_size",
        % "element_num_min" and "element_num_max".
        st1 = model.volumes{vol_j}.stations(1);
        st2 = model.volumes{vol_j}.stations(2);
        st1_pos = model.stations(st1);
        st2_pos = model.stations(st2);
        num_int_el = ceil(abs(st2_pos - st1_pos) / model.volumes{vol_j}.element_size);
        if num_int_el < model.volumes{vol_j}.element_num_min
            num_int_el = model.volumes{vol_j}.element_num_min;
        else
            if num_int_el > model.volumes{vol_j}.element_num_max
                num_int_el = model.volumes{vol_j}.element_num_max;
            end
        end
        element_counter = element_counter + num_int_el;
        num_int_nodes = num_int_el - 1;
        
        % Create the internal nodes
        temp_node_list = linspace(st1_pos, st2_pos, num_int_nodes + 2);
        int_nodes_pos{vol_j} = temp_node_list(2:(end-1));
        % Index the internal nodes.
        model.volumes{vol_j}.num_int_elements = num_int_el;
        model.volumes{vol_j}.int_nodes = (node_counter+1):1:(node_counter+num_int_nodes);
        node_counter = node_counter + num_int_nodes;
    end
    
    num_elements = element_counter;
    total_nodes_pos = cat(2, int_nodes_pos{:});
    total_nodes_pos = [model.stations, total_nodes_pos];
    
    % Sort the nodes in ascending order.
    [sorted_nodes_pos, idx_s] = sort(total_nodes_pos);
    [~, idx_u] = sort(idx_s);
    
    % Update the station nodes indexing
    station_nodes = idx_u(station_nodes);
    
    % Update the nodes indexing.
    for vol_j = 1:model.num_volumes
        % Update the internal nodes.
        model.volumes{vol_j}.int_nodes = idx_u(model.volumes{vol_j}.int_nodes);
    end
    
    % Define the elements. Each row contains the element's nodes.
    elements = zeros(element_counter, 2);
    
    element_counter = 0;
    for vol_j = 1:model.num_volumes
        % Recover the internal nodes.
        vol_int_nodes = model.volumes{vol_j}.int_nodes;
        
        % Include the station nodes.
        vol_st_nodes = station_nodes(model.volumes{vol_j}.stations);
        vol_nodes = [vol_st_nodes(1), vol_int_nodes, vol_st_nodes(2)];
        vol_num_nodes = length(vol_nodes);
        
        % Define the internal elements
        vol_int_el = zeros(1, vol_num_nodes - 1);
        for el_j = 1:(vol_num_nodes-1)
            element_counter = element_counter + 1;
            vol_int_el(el_j) = element_counter;
            elements(element_counter, :) = [vol_nodes(el_j), vol_nodes(el_j+1)];
        end
        model.volumes{vol_j}.int_elements = vol_int_el;
    end
    
    % Insert the disk elements
    elements_disk = zeros(1, model.num_disks);
    for disk_j = 1:model.num_disks
        elements_disk(disk_j) = station_nodes(model.disks{disk_j}.stations);
    end
    
    % Insert the bearing elements
    elements_bearing = zeros(1, model.num_bearings);
    for bearing_j = 1:model.num_bearings
        elements_bearing(bearing_j) = station_nodes(model.bearings{bearing_j}.stations);
    end
    
    num_nodes = length(sorted_nodes_pos);
    mesh.num_nodes = num_nodes;
    mesh.nodes_positions = sorted_nodes_pos;
    mesh.station_nodes = station_nodes;
    
    mesh.num_elements = num_elements;
    mesh.elements = elements;
    
    mesh.elements_disk = elements_disk;
    mesh.elements_bearing = elements_bearing;
    
    model.mesh = mesh;
    
    x_n = model.mesh.nodes_positions;
    y_n = zeros(size(x_n));
    
    x_s = model.stations;
    y_s = zeros(size(x_s));
    
    x_d = sorted_nodes_pos(elements_disk);
    y_d = zeros(size(x_d));
    
    x_b = sorted_nodes_pos(elements_bearing);
    y_b = zeros(size(x_b));
    
    fig_mesh = figure('OuterPosition', [0, 50, 1000, 300]);
    set(fig_mesh, 'PaperUnits', 'centimeters');
    set(fig_mesh, 'PaperSize', [16 5]);
    set(fig_mesh, 'PaperPositionMode', 'manual');
    set(fig_mesh, 'PaperPosition', [0 0 16 5]);
    ax_mesh = axes('NextPlot', 'add');
    set(ax_mesh, 'XLim', [-50, 550], 'XTick', -50:50:550)
    set(ax_mesh, 'YLim', [-100, 100], 'YTick', -100:50:100)
    
    % Plot the bearings
    plot(1000*x_b, y_b-6, '^', 'Color', [0 .8 .8], 'MarkerFaceColor', [0 .8 .8], 'MarkerSize', 5);
    % Plot the shaft
    plot(1000*x_n, y_n, '-', 'Color', [.6 .6 .7], 'LineWidth', 2)
    % Plot the disks
    plot(1000*x_d, y_d, 's', 'Color', [.8 .6 .4], 'MarkerFaceColor', [.8 .6 .4], 'MarkerSize', 5);
    % Plot the nodes
    plot(1000*x_n, y_n, '.k', 'LineWidth', 1, 'MarkerSize', 3)
    % Plot the stations
    plot(1000*x_s, y_s, '+', 'Color', [.7 .0  0], 'LineWidth', 1, 'MarkerSize', 3);
    % Add node numbers at station
    text(1000*x_s, y_s+15, num2cell(station_nodes), ...
        'Color', [.7  0  0], 'FontSize', 8);
    
    set(ax_mesh, 'NextPlot', 'replaceChildren');
    legend('Location', 'East', ...
        {'bearing', 'shaft', 'disk', 'node', 'station'})
    xlabel('X (mm)')
    ylabel('Y (mm)')
    
    set(ax_mesh, 'LooseInset', get(ax_mesh, 'TightInset'));
    
    plot_flags = [0, 1, 2, 3];
    save_figure(fig_mesh, 'Mesh', model.fig_dir, plot_flags);
end
function plot_FRF(model, M_F, G_F, C_F, K_F, load, load_node, f_vec)
    dim = size(K_F);
    T = 0.5*[  1 ,  1i; ...
              -1i,  1];
    T_g = zeros(dim);
    
    num_samples = length(f_vec);
    Omega_vec = 2*pi*f_vec;
    
    num_nodes = model.mesh.num_nodes;
    node_DOFs = model.node_DOFs;
    
    for node_j = 1:num_nodes
        offset = (node_j - 1) * node_DOFs;
        ind = [1, 2] + offset;
        T_g(ind, ind) = T;
        ind = [3, 4] + offset;
        T_g(ind, ind) = T;
    end
    
    % Vector of loads
    F_F = zeros(size(K_F, 1), 1);
    load_idx = (load_node - 1) * node_DOFs + (1:2);
    F_F(load_idx) = load;
    G_f = T_g * F_F;
    
    % Separate the V and W DOFs from the others
    ind_v = zeros(1, num_nodes);
    ind_w = zeros(1, num_nodes);
    ind_v(1) = 1;
    ind_w(1) = 2;
    for j = 2:num_nodes
        ind_v(j) = ind_v(j-1) + node_DOFs;
        ind_w(j) = ind_w(j-1) + node_DOFs;
    end
    
    FRF_all = zeros(dim(1), num_samples);
    FRF_QV = zeros(num_nodes, num_samples);
    FRF_QW = zeros(num_nodes, num_samples);
    for w_j = 1:num_samples
        D_F = -Omega_vec(w_j) * G_F;
        if ~isempty(C_F)
            D_F = D_F + C_F;
        end
        FRF_all(:, w_j) = FRF_unbalanced(M_F, D_F, K_F, G_f, Omega_vec(w_j));
        FRF_QV(:, w_j) = 2*FRF_all(ind_v, w_j);
        FRF_QW(:, w_j) = 2*FRF_all(ind_w, w_j);
    end
    
    [X, Y] = meshgrid(f_vec, 1:num_nodes);
    
    % Plot V ODS
    fig_ODS_V = figure('OuterPosition', [0, 50, 500, 500]);
    set(fig_ODS_V, 'PaperUnits', 'centimeters')
    set(fig_ODS_V, 'PaperSize', [16 16])
    set(fig_ODS_V, 'PaperPositionMode', 'manual')
    set(fig_ODS_V, 'PaperPosition', [0 0 16 16])
    ax_ODS_V = axes;
    waterfall(X', Y', abs(FRF_QV)')
    % surf(X, Y, real(FRF_V));
    % shading flat
    set(ax_ODS_V, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(copper(128))
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('V(\Omega)')
    set(ax_ODS_V, 'LooseInset', get(ax_ODS_V, 'TightInset'))
    plot_flags = [0, 1, 2, 3];
    save_figure(fig_ODS_V, 'ODS_V', model.fig_dir, plot_flags)
    
    % Plot W ODS
    fig_ODS_W = figure('OuterPosition', [500, 50, 500, 500]);
    set(fig_ODS_W, 'PaperUnits', 'centimeters')
    set(fig_ODS_W, 'PaperSize', [16 16])
    set(fig_ODS_W, 'PaperPositionMode', 'manual')
    set(fig_ODS_W, 'PaperPosition', [0 0 16 16])
    ax_ODS_W = axes;
    waterfall(X', Y', abs(FRF_QW)');
    % surf(X, Y, real(FRF_W));
    % shading flat
    set(ax_ODS_W, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(copper(128))
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('W(\Omega)')
    set(ax_ODS_W, 'LooseInset', get(ax_ODS_W, 'TightInset'))
    plot_flags = [0, 1, 2, 3];
    save_figure(fig_ODS_W, 'ODS_W', model.fig_dir, plot_flags)
    
    FRF_V_dB = 20*log10(abs(FRF_QV));
    FRF_W_dB = 20*log10(abs(FRF_QW));
    % Subtract the angle from the spatial phase
    load_ang = angle(load + 1i*[-load(2), load(1)]);
    FRF_V_ang = unwrap(angle(FRF_QV)+load_ang(1), [], 1) * 180 / pi;
    FRF_W_ang = unwrap(angle(FRF_QW)+load_ang(2), [], 1) * 180 / pi;
    
    fig_FRF_V = figure('OuterPosition', [0, 50, 1000, 500]);
    set(fig_FRF_V, 'PaperUnits', 'centimeters');
    set(fig_FRF_V, 'PaperSize', [24 12]);
    set(fig_FRF_V, 'PaperPositionMode', 'manual');
    set(fig_FRF_V, 'PaperPosition', [0 0 24 12]);
    ax_FRF_V_ang = subplot(1, 2, 1);
    surf(X, Y, FRF_V_dB);
    set(ax_FRF_V_ang, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('|V(\Omega)| (dB)')
    
    ax_FRF_V_ang = subplot(1, 2, 2);
    surf(X, Y, FRF_V_ang);
    set(ax_FRF_V_ang, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    set(ax_FRF_V_ang, 'ZTick', -720:90:720)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('\angle V(\Omega) (deg)')
    plot_flags = [0, 3, 4];
    
    save_figure(fig_FRF_V, 'FRF_V', model.fig_dir, plot_flags)
    
    % Plot the W FRF
    fig_FRF_W = figure('OuterPosition', [0, 50, 1000, 500]);
    set(fig_FRF_W, 'PaperUnits', 'centimeters');
    set(fig_FRF_W, 'PaperSize', [24 12]);
    set(fig_FRF_W, 'PaperPositionMode', 'manual');
    set(fig_FRF_W, 'PaperPosition', [0 0 24 12]);
    
    ax_FRF_W_dB = subplot(1, 2, 1);
    surf(X, Y, FRF_W_dB);
    set(ax_FRF_W_dB, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('|W| (\Omega) (dB)')
    
    ax_FRF_V_ang = subplot(1, 2, 2);
    surf(X, Y, FRF_W_ang);
    set(ax_FRF_V_ang, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    set(ax_FRF_V_ang, 'ZTick', -720:90:720)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('\angle W(\Omega) (deg)')
    
    plot_flags = [0, 3, 4];
    save_figure(fig_FRF_V, 'FRF_W', model.fig_dir, plot_flags)
    
    fig_FRF_disk1 = figure('OuterPosition', [0, 50, 600, 600]);
    set(fig_FRF_disk1, 'PaperUnits', 'centimeters');
    set(fig_FRF_disk1, 'PaperSize', [16 16]);
    set(fig_FRF_disk1, 'PaperPositionMode', 'manual');
    set(fig_FRF_disk1, 'PaperPosition', [0 0 16 16]);
    
    disk_station = model.disks{1}.stations;
    disk_node = model.mesh.station_nodes(disk_station);
    
    ax_FRF_disk1_mag = subplot(2, 1, 1);
    plot(f_vec, FRF_V_dB(disk_node, :), ...
        'Color', [0 .7 0])
    set(ax_FRF_disk1_mag, 'NextPlot', 'add');
    plot(f_vec, FRF_W_dB(disk_node, :), ...
        'Color', [0 0 .7])
    set(ax_FRF_disk1_mag, 'NextPlot', 'replaceChildren');
    set(ax_FRF_disk1_mag, 'GridLineStyle', '-');
    set(ax_FRF_disk1_mag, 'LineWidth', .25);
    set(ax_FRF_disk1_mag, 'XColor', [.2 .2 .2]);
    set(ax_FRF_disk1_mag, 'YColor', [.2 .2 .2]);
    grid on
    ylabel('Magnitude (dB)')
    legend('V(\Omega)', 'W(\Omega)');
    
    ax_FRF_disk1_ang = subplot(2, 1, 2);
    plot(f_vec, FRF_V_ang(disk_node, :), ...
        'Color', [0 .7 0])
    set(ax_FRF_disk1_ang, 'NextPlot', 'add');
    plot(f_vec, FRF_W_ang(disk_node, :), ...
        'Color', [0 0 .7])
    set(ax_FRF_disk1_ang, 'NextPlot', 'replaceChildren');
    ylabel('Phase (deg)')
    xlabel('\Omega (Hz)')
    set(ax_FRF_disk1_ang, 'YTick', -720:90:720)
    set(ax_FRF_disk1_ang, 'GridLineStyle', '-');
    set(ax_FRF_disk1_ang, 'LineWidth', .25);
    set(ax_FRF_disk1_ang, 'XColor', [.2 .2 .2]);
    set(ax_FRF_disk1_ang, 'YColor', [.2 .2 .2]);
    grid on
    
    plot_flags = [0, 1, 2, 3];
    save_figure(fig_FRF_disk1, 'FRF_disk1', model.fig_dir, plot_flags)
end

function FRF = FRF_unbalanced(M_F, D_F, K_F, G_f, Omega)
    % Return the FRF column for frequency Omega
    mat = -Omega^2 * M_F + 1i*Omega*D_F + K_F;
    FRF = mat\G_f;
end
function M_B_e = el_bearing2_mt(model, bearing_idx)
    M_B_e = model.bearings{bearing_idx}.M;
end
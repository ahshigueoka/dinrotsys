function C_F = assemble_matrix_damping(model)
    % Preallocate the global matrix
    total_num_nodes = model.mesh.num_nodes*model.node_DOFs;
    C_F = zeros(total_num_nodes, total_num_nodes);
    
    % Add the damping terms from the bearings
    for bearing_j = 1:model.num_bearings
        C_b = el_bearing2_c(model, bearing_j);
        
        n1 = model.mesh.elements_bearing(bearing_j);
        node_offset_a = (n1-1)*model.node_DOFs;
        element_nodes_a = 1:model.node_DOFs;
        global_nodes_a = node_offset_a + (1:model.node_DOFs);
        C_F(global_nodes_a, global_nodes_a) = ...
            C_F(global_nodes_a, global_nodes_a) ...
            + C_b(element_nodes_a, element_nodes_a);
    end
end
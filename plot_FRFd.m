function plot_FRFd(model, M_F, G_F, C_F, K_F, F_F, f_vec)
    dim = size(K_F);
    T = 0.5*[   1,   1;  -1i, 1i];
    Ti = inv(T);
    T_g = zeros(dim);
    Ti_g = zeros(dim);
    
    num_samples = length(f_vec);
    Omega_vec = 2*pi*f_vec;
    
    num_nodes = model.mesh.num_nodes;
    node_DOFs = model.node_DOFs;
    for node_j = 1:num_nodes
        offset = (node_j - 1) * node_DOFs;
        ind = [1, 2] + offset;
        T_g(ind, ind) = T;
        Ti_g(ind, ind) = Ti;
        ind = [3, 4] + offset;
        T_g(ind, ind) = T;
        Ti_g(ind, ind) = Ti;
    end
         
    M_C = Ti_g * M_F * T_g;
    K_C = Ti_g * K_F * T_g;
    F_C = Ti_g * F_F;
    
    % Separate the V and W DOFs from the others
    ind_v = zeros(1, num_nodes);
    ind_w = zeros(1, num_nodes);
    ind_v(1) = 1;
    ind_w(1) = 2;
    for j = 2:num_nodes
        ind_v(j) = ind_v(j-1) + node_DOFs;
        ind_w(j) = ind_w(j-1) + node_DOFs;
    end
    
    FRF_PPcQQc = zeros(dim(1), num_samples);
    % FRF_V = zeros(num_nodes, num_samples);
    % FRF_W = zeros(num_nodes, num_samples);
    for w_j = 1:num_samples
        D_C = -Omega_vec(w_j) * Ti_g * G_F * T_g;
        if ~isempty(C_F)
            D_C = D_C + Ti_g * C_F * T_g;
        end
        FRF_PPcQQc(:, w_j) = FRF_d(M_C, D_C, K_C, F_C, Omega_vec(w_j));
        FRF_Hpg = FRF_PPcQQc(ind_v, :);
        FRF_Hpcg = FRF_PPcQQc(ind_w, :);
        % FRF_VWBL = T_g * FRF_PPcQQc;
        % FRF_V(:, w_j) = FRF_VWBL(ind_v, w_j);
        % FRF_W(:, w_j) = FRF_VWBL(ind_w, w_j);
    end
    
    [X, Y] = meshgrid(f_vec, 1:num_nodes);
    
%     figure
%     % Plot V ODS
%     surf(X, Y, real(FRF_V));
%     shading interp
%     xlabel('\Omega (Hz)')
%     ylabel('Node number')
%     zlabel('V(\Omega)')
%     title('ODS - V')
%     % waterfall(X, Y, abs(FRF_V))
%     
%     figure
%     % Plot W ODS
%     surf(X, Y, real(FRF_W));
%     shading interp
%     xlabel('\Omega (Hz)')
%     ylabel('Node number')
%     zlabel('W(\Omega)')
%     
%     title('ODS - W')
%     % waterfall(X, Y, abs(FRF_W));
    
    double_colormap = [linspace(0, 1, 128)' zeros(128, 1) zeros(128, 1); ...
                       zeros(128, 1) linspace(0, 1, 128)' zeros(128, 1)];
                   
    FRF_Hpg_mag = 20*log10(abs(FRF_Hpg));
    FRF_Hpcg_mag = 20*log10(abs(FRF_Hpcg));
    
    Cpgdata = FRF_Hpg_mag - 200;
    Cpcgdata = FRF_Hpcg_mag;
    
    % Plot the FRF in V
    fig_FRF_surf = figure('OuterPosition', [0, 50, 1000, 500]);
    set(fig_FRF_surf, 'PaperUnits', 'centimeters');
    set(fig_FRF_surf, 'PaperSize', [24 12]);
    set(fig_FRF_surf, 'PaperPositionMode', 'manual');
    set(fig_FRF_surf, 'PaperPosition', [0 0 24 12]);
    
    ax_FRF_surf = subplot(1, 2, 1);
    FRFd_V = FRF_Hpg + FRF_Hpcg;
    surf(X, Y, 20*log10(abs(FRFd_V)))
    colormap(gray(128)*.8)
    shading interp
    set(ax_FRF_surf, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('|V|(\Omega), dFRF method')
    
    ax_FRF_surf = subplot(1, 2, 2);
    FRFd_V_ang = unwrap(angle(FRFd_V), [], 1) * 180 / pi;
    surf(X, Y, FRFd_V_ang)
    colormap(gray(128)*.8)
    shading interp
    set(ax_FRF_surf, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    set(ax_FRF_surf, 'ZTick', -720:90:720)
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('\angle V(\Omega), dFRF method')
    
    plot_flags = [0, 3, 4];
    save_figure(fig_FRF_surf, 'FRF_V_dir', model.fig_dir, plot_flags)
    
    % Plot the FRF in W
    fig_FRF_surf = figure('OuterPosition', [0, 50, 1000, 500]);
    set(fig_FRF_surf, 'PaperUnits', 'centimeters');
    set(fig_FRF_surf, 'PaperSize', [24 12]);
    set(fig_FRF_surf, 'PaperPositionMode', 'manual');
    set(fig_FRF_surf, 'PaperPosition', [0 0 24 12]);
    
    ax_FRF_surf = subplot(1, 2, 1);
    FRFd_W = FRF_Hpg - FRF_Hpcg;
    surf(X, Y, 20*log10(abs(FRFd_W)))
    colormap(gray(128)*.8)
    shading interp
    set(ax_FRF_surf, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('|W|(\Omega), dFRF method')
    
    ax_FRF_surf = subplot(1, 2, 2);
    FRFd_W_ang = unwrap(angle(FRFd_W), [], 1) * 180 / pi;
    surf(X, Y, FRFd_W_ang)
    colormap(gray(128)*.8)
    shading interp
    set(ax_FRF_surf, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    set(ax_FRF_surf, 'ZTick', -720:90:720)
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('\angle W(\Omega), dFRF method')
    
    plot_flags = [0, 3, 4];
    save_figure(fig_FRF_surf, 'FRF_W_dir', model.fig_dir, plot_flags)
    
    % Plot the forward and backwards FRFs
    fig_FRF_surf = figure('OuterPosition', [0, 50, 500, 500]);
    set(fig_FRF_surf, 'PaperUnits', 'centimeters');
    set(fig_FRF_surf, 'PaperSize', [16 16]);
    set(fig_FRF_surf, 'PaperPositionMode', 'manual');
    set(fig_FRF_surf, 'PaperPosition', [0 0 16 16]);
    ax_FRF_surf = axes;
    surf(X, Y, FRF_Hpg_mag, Cpgdata)
    hold on
    surf(X, Y, FRF_Hpcg_mag, Cpcgdata)
    hold off
    colormap(double_colormap)
    caxis([-400 0]);
    shading interp
    set(ax_FRF_surf, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('$(H_{pg}, H_{\bar{p}g})(\Omega)$', 'Interpreter', 'latex')
    title('$H_{pg}(red), H_{\bar{p}g}(green)$', 'Interpreter', 'latex')
    set(ax_FRF_surf, 'LooseInset', get(ax_FRF_surf, 'TightInset'));
    plot_flags = [0, 3, 4];
    save_figure(fig_FRF_surf, 'FRF_dir', model.fig_dir, plot_flags)
    view(0, 90)
    save_figure(fig_FRF_surf, 'FRF_dir_top', model.fig_dir, plot_flags)
    
    % FRF at the disk 1 node
    fig_FRF_disk1 = figure('OuterPosition', [0, 50, 1000, 500]);
    set(fig_FRF_disk1, 'PaperUnits', 'centimeters');
    set(fig_FRF_disk1, 'PaperSize', [16 8]);
    set(fig_FRF_disk1, 'PaperPositionMode', 'manual');
    set(fig_FRF_disk1, 'PaperPosition', [0 0 16 8]);
    ax_FRF_disk1 = axes('NextPlot', 'add');
    
    disk_station = model.disks{1}.stations;
    disk_node = model.mesh.station_nodes(disk_station);
    plot(f_vec, FRF_Hpg_mag(disk_node, :), ...
        'Color', [.7 0 0])
    plot(f_vec, FRF_Hpcg_mag(disk_node, :), ...
        'Color', [0 .7 0])
    set(ax_FRF_disk1, 'NextPlot', 'replaceChildren');
    set(ax_FRF_disk1, 'GridLineStyle', '-');
    set(ax_FRF_disk1, 'LineWidth', .25);
    set(ax_FRF_disk1, 'XColor', [.2 .2 .2]);
    set(ax_FRF_disk1, 'YColor', [.2 .2 .2]);
    grid on
    ylabel('Magnitude (dB)')
    legend('V(\Omega)', 'W(\Omega)');
    
    xlabel('\Omega (Hz)')
    ylabel('dFRF at disk n� 1')
    
    leg_h = legend('Hpg', 'Hpcg');
    set(leg_h, 'Interpreter', 'latex', 'FontSize', 12, ...
        'String', {'$H_{pg}$', '$H_{\bar{p}g}$'})
    
    plot_flags = [0, 1, 2, 3];
    save_figure(fig_FRF_disk1, 'FRF_dir_disk1', model.fig_dir, plot_flags)
    
%     FRF_V_dB = 20*log10(abs(FRF_V));
%     FRF_W_dB = 20*log10(abs(FRF_W));
%     
%     figure
%     axes
%     hold on
%     surf(X, Y, FRF_V_dB);
%     hold off
%     colormap(gray(256)*0.8)
%     shading interp
%     xlabel('\Omega (Hz)')
%     ylabel('Node number')
%     zlabel('V(\Omega) (dB)')
%     title('FRF - V')
%     
%     figure
%     surf(X, Y, FRF_W_dB);
%     colormap(gray(256)*0.8)
%     shading interp
%     xlabel('\Omega (Hz)')
%     ylabel('Node number')
%     zlabel('W(\Omega) (dB)')
%     title('FRF - W')
end

function FRF = FRF_d(M_C, D_C, K_C, F_C, Omega)
    % Columns corresponding to the perturbation
    k = find(F_C);
    
    % Only get the column equivalent to forward perturbation.
    choose_col = zeros(size(K_C, 1), 1);
    choose_col(k(1)) = 1;
    
    % Return the FRF column for this frequency
    mat = -Omega^2 * M_C + 1i*Omega*D_C + K_C;
    FRF = mat\choose_col;
end
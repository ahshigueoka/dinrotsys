function C_b = el_bearing2_c(model, bearing_idx)
    C_b = model.bearings{bearing_idx}.C;
end
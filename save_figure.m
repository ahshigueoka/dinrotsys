function save_figure(fig_h, filename, fig_dir, flags)
    filepath = sprintf('%s/%s', fig_dir, filename);
    plot_flag = true;
    fast_flag = false;
    
    % Save figures in FIG, PNG, PDF and color EPS level 3 file formats
    if plot_flag
    num_flags = length(flags);
    for f_j = 1:num_flags
        if fast_flag
            plot_type = 1;
        else
            plot_type = flags(f_j);
        end
        switch plot_type
            case 0
                saveas(fig_h, filepath, 'fig')
            case 1
                print('-dpdf', filepath)
            case 2
                print('-depsc', filepath)
            case 3
                print('-dpng', '-r300', filepath)
            case 4
                print('-djpeg', '-r300', filepath)
            otherwise
                warning('Ignoring file format option: %d\\n', f)
        end
    end
    end
end
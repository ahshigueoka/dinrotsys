function nelson_paper_script()
    % assignment_script
    %
    % This script reads in the material and geometric properties defined
    % in file "data_file" and carries on a dynamic analysis of the shaft
    % described in the assignment task sheet modeled after the finite
    % element method formulation described in
    % (NELSON HD; McVaugh JM; 1976), after having made the following
    % correction:
    % 
    %       [   0   0   0   0 ]
    % G^d = [   0   0   0   0 ]
    %       [   0   0   0 -Ip ]
    %       [   0   0  Ip   0 ]
    % 
    % Detailed steps:
    %   - create the finite element mesh
    %   - assemble the finite element global matrix
    %   - apply the boundary conditions
    %   - convert to state space formulation
    %   - calculate the eigenvalues and eigenvectors
    %   - calculate Campbell's Chart
    %   - convert loads to the rotating frame R
    %   - calculate the directional FRFs.

    close all
    
    E_steel = 2.078e11;
    rho_steel = 7806;

    %% The disk's properties
    m_d = 1.401;
    I_p = .0020;
    I_d = .0136;
    
    %% The bearing's properties
    % Mass inertia at each direction
    % M_b = [ M_VV  M_VW ]
    %       [ M_WV  M_WW ]
    M_b = zeros(4);
    
    % Stiffness at each direction
    % K_b = 4x4
    K_b = zeros(4);
    K_b(1, 1) = 4.378e7;
    K_b(2, 2) = 3.503e7;
    
    % Damping of the bearings
    % Ignoring damping
    C_b = zeros(4);
    C_b(1, 1) = 1e-6;
    C_b(2, 2) = 1e-6;
    C_b(3, 3) = 1e-6;
    C_b(4, 4) = 1e-6;
    
    %% Define the shaft's main stations
    st = [     0,  1.27,  5.08,  7.62,  8.89, 10.16, 10.67, 11.43, ...
           12.70, 13.46, 16.51, 19.05, 22.86, 26.67, 28.70, 30.48, ...
           31.50, 34.54, 38.10]*1e-2;
    
    model.num_stations = size(st, 2);
    model.stations = st;
    clear st
    
    %% Define the materials
    
    % Material 1: steel
    material = cell(1, 1);
    mat_prop.E = E_steel;
    mat_prop.rho = rho_steel;
    material{1} = mat_prop;
    clear mat_prop
    
    model.num_materials = 1;
    model.material = material;
    clear material
    
    %% Define the volume geometries
    geometry_data = cell(18, 1);
    
    r_o = [0.51, 1.02, 0.76, 2.03, 2.03, 3.30, 3.30, 2.54, 2.54, 1.27, ...
           1.27, 1.52, 1.52, 1.27, 1.27, 3.81, 2.03, 2.03]*1e-2 / 2;
    r_i = [   0,    0,    0,    0,    0,    0, 1.52, 1.78,    0,    0, ...
              0,    0,    0,    0,    0,    0,    0, 1.52]*1e-2 / 2;
    L   = diff(model.stations);
    model.num_geometries = size(L, 2);
    
    for vol_j = 1:model.num_geometries
        geometry_data{vol_j}.r_o = r_o(vol_j);
        geometry_data{vol_j}.r_i = r_i(vol_j);
        geometry_data{vol_j}.L   = L(vol_j);
    end
    clear r_o r_i L
    
    model.geometry_data = geometry_data;
    
    %% Define the shaft's volumes
    model.num_volumes = 18;
    volumes = cell(model.num_volumes, 1);
    
    for vol_j = 1:model.num_volumes
        volumes{vol_j}.material = 1;
        volumes{vol_j}.geometry = vol_j;
        volumes{vol_j}.stations = [vol_j, vol_j + 1];
        volumes{vol_j}.element_type = 'shaft2';
        volumes{vol_j}.element_size = 5e-3;
        volumes{vol_j}.element_num_min = 2;
        volumes{vol_j}.element_num_max = 100;
    end
    
    model.volumes = volumes;
    clear volumes
    
    %% Add disks
    model.num_disks = 1;
    disks = cell(model.num_disks, 1);
    
    prop.m = m_d;
    prop.I_p = I_p;
    prop.I_d = I_d;
    prop.stations = 5;
    disks{1} = prop;
    clear prop
    
    model.disks = disks;
    clear disks
    
    %% Add the bearings
    model.num_bearings = 2;
    bearings = cell(model.num_bearings, 1);
    
    % Left bearing
    bear.M = M_b;
    bear.K = K_b;
    bear.C = C_b;
    bear.stations = 11;
    bear.constrained_gdls = [5, 6];
    bearings{1} = bear;
    clear bear
    
    % Right bearing
    bear.M = M_b;
    bear.K = K_b;
    bear.C = C_b;
    bear.stations = 15;
    bear.constrained_gdls = [5, 6];
    bearings{2} = bear;
    clear bear
    
    model.bearings = bearings;
    clear bearings
    
    % Clear unnecessary parameters
    clear C_b E_steel I_d I_p K_b M_b m_d rho_steel vol_j
    
    %% Generate the mesh
    model.node_DOFs = 4;
    model = generate_mesh(model);
    
    %% Assemble the global inertia matrix in the fixed frame coordinates
    M_F = assemble_matrix_inertia(model);
    
    %% Assemble the global gyroscopic matrix in the fixed frame coordinates
    G_F = assemble_matrix_gyroscopic(model);
    
    %% Assemble the global damping matrix in the fixed frame coordinates
    % C_F = assemble_matrix_damping(model);
    
    %% Assemble the global stiffness matrix in the fixed frame coordinates
    K_F = assemble_matrix_stiffness(model);
    
    %% Vector of loads, in fixed frame coordinates
    F_F = zeros(model.mesh.num_nodes*model.node_DOFs, 1);
    
    %% Apply the boundary conditions
    % Node, DOF, value
    % Include in ascending order of nodes, and for each node in ascending
    % order of DOF.
%     first_node = 1;
%     last_node = model.mesh.num_nodes;
%     bcs = [ first_node, 3, 0;
%             first_node, 4, 0;
%              last_node, 3, 0;
%              last_node, 4, 0];
%     model.num_bcs = 4;
%     model.bcs = bcs;
%     clear bcs
%     
%     [M_F, G_F, ~, K_F, F_F] = apply_boundary_conditions(model, M_F, G_F, [], K_F, F_F);
    
    %% Convert to the state space formulation
    Omega = 0;
    % A = sparse([-M_F zeros(size(K_F)); zeros(size(K_F)) K_F]);
    % B = sparse([zeros(size(K_F)) M_F; M_F -Omega*G_F]);
    % [mode_shapes, omega_n] = eigs(B, A, 10, 0);
    
    %% Calculate the natural frequencies
    K_s = sparse(K_F);
    M_s = sparse(M_F);
    [V, lambda] = eigs(K_s, M_s, 10, 0);
    % eliminate residual terms
    V(abs(V) < 1e-6) = 0;
    
    dis_num_DOFs = model.mesh.num_nodes;
    ind_modes_y = zeros(1, dis_num_DOFs);
    ind_modes_z = zeros(1, dis_num_DOFs);
    ind_modes_y(1:2) = [1, 3];
    ind_modes_z(1:2) = [2, 4];
    for j = 3:dis_num_DOFs
        ind_modes_y(j) = ind_modes_y(j-1) + 4;
        ind_modes_z(j) = ind_modes_z(j-1) + 4;
    end
    
    close all
    for mode_j = 1:10
        figure
        subplot(2, 1, 1)
        plot([1, model.mesh.num_nodes], [0, 0], 'k')
        hold on
        amp = max(abs(V((ind_modes_y), mode_j)));
        % plot(V((ind_modes_y), mode_j)/amp, 'b')
        plot(V(1:model.node_DOFs:((model.mesh.num_nodes-1)*model.node_DOFs+1), mode_j))
        hold off
        axis([1, model.mesh.num_nodes -1.5 1.5])
        title(sprintf('Mode %d, y', mode_j))
        subplot(2, 1, 2)
        plot([1, model.mesh.num_nodes], [0, 0], 'k')
        hold on
        amp = max(abs(V((ind_modes_z), mode_j)));
        % plot(V(ind_modes_z, mode_j)/amp, 'b')
        plot(V(2:4:((model.mesh.num_nodes-1)*model.node_DOFs+2), mode_j))
        hold off
        axis([1, model.mesh.num_nodes -1.5 1.5])
        title(sprintf('Mode %d, z', mode_j))
    end
    
    
    w = sqrt(diag(lambda)) / (2 *pi) * 60;
    
    format shorte
    fprintf('Natural frequencies, RPM\n')
    disp(w(1:10))
end
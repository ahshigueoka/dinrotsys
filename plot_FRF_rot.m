function plot_FRF_rot(model, M_F, G_F, C_F, K_F, F_c, F_s, node_idx, f_vec)
    dim = size(K_F);
    
    num_samples = length(f_vec);
    Omega_vec = 2*pi*f_vec;
    
    num_nodes = model.mesh.num_nodes;
    node_DOFs = model.node_DOFs;
    
    G_c = zeros(dim(1), 1);
    G_s = zeros(dim(1), 1);
    dof_1 = (node_idx-1)*node_DOFs + 1;
    dof_2 = (node_idx-1)*node_DOFs + 2;
    G_c((node_idx-1)*node_DOFs + (1:node_DOFs)) = F_c;
    G_s((node_idx-1)*node_DOFs + (1:node_DOFs)) = F_s;
    
    % Separate the V and W DOFs from the others
    ind_v = zeros(1, num_nodes);
    ind_w = zeros(1, num_nodes);
    ind_v(1) = 1;
    ind_w(1) = 2;
    for j = 2:num_nodes
        ind_v(j) = ind_v(j-1) + node_DOFs;
        ind_w(j) = ind_w(j-1) + node_DOFs;
    end
    
    FRF_all = zeros(dim(1), num_samples);
    FRF_V = zeros(num_nodes, num_samples);
    FRF_W = zeros(num_nodes, num_samples);
    for w_j = 1:num_samples
        D_F = -Omega_vec(w_j) * G_F;
        if ~isempty(C_F)
            D_F = D_F + C_F;
        end
        q_cs = FRF_unbalanced(M_F, D_F, K_F, G_c, G_s, Omega_vec(w_j));
        % Transform the cosine, sine components into complex numbers
        FRF_all(:, w_j) = q_cs(1:dim(1)) + 1i*q_cs((1:dim(1))+dim(1));
        FRF_V(:, w_j) = FRF_all(ind_v, w_j);
        FRF_W(:, w_j) = FRF_all(ind_w, w_j);
    end
    
    [X, Y] = meshgrid(f_vec, 1:num_nodes);
    
    % Plot V ODS
    fig_ODS_V = figure('OuterPosition', [0, 50, 500, 500]);
    set(fig_ODS_V, 'PaperUnits', 'centimeters')
    set(fig_ODS_V, 'PaperSize', [16 16])
    set(fig_ODS_V, 'PaperPositionMode', 'manual')
    set(fig_ODS_V, 'PaperPosition', [0 0 16 16])
    ax_ODS_V = axes;
    waterfall(X', Y', abs(FRF_V)')
    % surf(X, Y, real(FRF_V));
    % shading flat
    set(ax_ODS_V, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(copper(128))
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('V(\Omega), trig method')
    set(ax_ODS_V, 'LooseInset', get(ax_ODS_V, 'TightInset'))
    plot_flags = [0, 1, 2, 3];
    save_figure(fig_ODS_V, 'ODS_V_tm', model.fig_dir, plot_flags)
    
    % Plot W ODS
    fig_ODS_W = figure('OuterPosition', [500, 50, 500, 500]);
    set(fig_ODS_W, 'PaperUnits', 'centimeters')
    set(fig_ODS_W, 'PaperSize', [16 16])
    set(fig_ODS_W, 'PaperPositionMode', 'manual')
    set(fig_ODS_W, 'PaperPosition', [0 0 16 16])
    ax_ODS_W = axes;
    waterfall(X', Y', abs(FRF_W)');
    % surf(X, Y, real(FRF_W));
    % shading flat
    set(ax_ODS_W, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(copper(128))
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('W(\Omega), trig method')
    set(ax_ODS_W, 'LooseInset', get(ax_ODS_W, 'TightInset'))
    plot_flags = [0, 1, 2, 3];
    save_figure(fig_ODS_W, 'ODS_W_tm', model.fig_dir, plot_flags)
    
    FRF_V_dB = 20*log10(abs(FRF_V));
    FRF_W_dB = 20*log10(abs(FRF_W));
    % Do not forget to subtract the spatial phase of the perturbation
    G_ang = angle(G_c + 1i*G_s);
    FRF_V_ang = unwrap(-angle(FRF_V) + G_ang(dof_1), [], 1) * 180 / pi;
    FRF_W_ang = unwrap(-angle(FRF_W) + G_ang(dof_2), [], 1) * 180 / pi;
    
    fig_FRF_V = figure('OuterPosition', [0, 50, 1000, 500]);
    set(fig_FRF_V, 'PaperUnits', 'centimeters');
    set(fig_FRF_V, 'PaperSize', [24 12]);
    set(fig_FRF_V, 'PaperPositionMode', 'manual');
    set(fig_FRF_V, 'PaperPosition', [0 0 24 12]);
    ax_FRF_V_ang = subplot(1, 2, 1);
    surf(X, Y, FRF_V_dB);
    set(ax_FRF_V_ang, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('|V(\Omega)| (dB), trig method')
    
    ax_FRF_V_ang = subplot(1, 2, 2);
    surf(X, Y, FRF_V_ang);
    set(ax_FRF_V_ang, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    set(ax_FRF_V_ang, 'ZTick', -720:90:720)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('\angle V(\Omega), trig method (deg)')
    plot_flags = [0, 3, 4];
    
    save_figure(fig_FRF_V, 'FRF_V_tm', model.fig_dir, plot_flags)
    
    % Plot the W FRF
    fig_FRF_W = figure('OuterPosition', [0, 50, 1000, 500]);
    set(fig_FRF_W, 'PaperUnits', 'centimeters');
    set(fig_FRF_W, 'PaperSize', [24 12]);
    set(fig_FRF_W, 'PaperPositionMode', 'manual');
    set(fig_FRF_W, 'PaperPosition', [0 0 24 12]);
    
    ax_FRF_W_dB = subplot(1, 2, 1);
    surf(X, Y, FRF_W_dB);
    set(ax_FRF_W_dB, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('|W| (\Omega) (dB), trig method')
    
    ax_FRF_V_ang = subplot(1, 2, 2);
    surf(X, Y, FRF_W_ang);
    set(ax_FRF_V_ang, 'YLim', [1 model.mesh.num_nodes], 'YTick', 1:4:model.mesh.num_nodes)
    set(ax_FRF_V_ang, 'ZTick', -720:90:720)
    colormap(gray(256)*0.8)
    shading interp
    xlabel('\Omega (Hz)')
    ylabel('Node number')
    zlabel('\angle W(\Omega) (deg), trig method')
    
    plot_flags = [0, 3, 4];
    save_figure(fig_FRF_V, 'FRF_W_tm', model.fig_dir, plot_flags)
end

function FRF = FRF_unbalanced(M_F, D_F, K_F, G_c, G_s, Omega)
    % Return the FRF column for frequency Omega
    mat = sparse([ (K_F-Omega^2*M_F),        Omega*D_F  ;...
                   -Omega*D_F, (K_F-Omega^2*M_F)]);
             
    FRF = full(mat\sparse([G_c; G_s]));
end
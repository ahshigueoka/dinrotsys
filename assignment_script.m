function assignment_script(data_file)
    % assignment_script
    %
    % This script reads in the material and geometric properties defined
    % in file "data_file" and carries on a dynamic analysis of the shaft
    % described in the assignment task sheet modeled after the finite
    % element method formulation described in
    % (NELSON HD; McVaugh JM; 1976), after having made the following
    % correction:
    % 
    %       [   0   0   0   0 ]
    % G^d = [   0   0   0   0 ]
    %       [   0   0   0 -Ip ]
    %       [   0   0  Ip   0 ]
    % 
    % Detailed steps:
    %   - create the finite element mesh
    %   - assemble the finite element global matrix
    %   - apply the boundary conditions
    %   - convert to state space formulation
    %   - calculate the eigenvalues and eigenvectors
    %   - calculate Campbell's Chart
    %   - convert loads to the rotating frame R
    %   - calculate the directional FRFs.

    close all
    load(data_file)
    
    model.fig_dir = './figures';
    %% Calculate the disk's properties
    m_d = rho_aco * pi * R_D ^ 2 * E_D;
    I_p = m_d * R_D ^ 2 / 2;
    I_d = m_d * R_D ^ 2 / 4;
    
    %% Calculate the bearing's properties
    % Mass inertia at each direction
    % M_b = [ M_VV  M_VW ]
    %       [ M_WV  M_WW ]
    % First term: mass of bearing house
    % Second term: mass of clamped beam extremity
    % Third term: equivalent mass of bar in extension mode
    M_b = zeros(4);
    M_b(1, 1) = rho_al * (L_m*B_m - pi*D_m^2/4) * E_m ...
                + 2 * rho_aco * B_m * B_L * E_L ...
                + 2 * rho_aco * L_L * B_L * E_L / 3;
    
    % First term: mass of bearing house
    % Second term: mass of clamped beam extremity
    % Third term: equivalent mass of beam in bending mode
    M_b(2, 2) = rho_al * (L_m*B_m - pi*D_m^2/4) * E_m ...
                + 2 * rho_aco * B_m * B_L * E_L ...
                + 2 * 156 / 420 * rho_aco * L_L * B_L * E_L;
    % Ignoring gyroscopic inertial terms
    m_block = rho_aco * B_L * B_m * L_m;
    m_hole = rho_aco * pi * D_m^2 / 4 * B_L;
    I_y_block = m_block * (B_L^2 + L_m^2);
    I_y_hole = m_hole * (3 * D_m^2 / 4 + B_L^2) / 12;
    I_z_block = m_block * (B_L^2 + B_m^2);
    I_z_hole = m_hole * (3 * D_m^2 / 4 + B_L^2) / 12;
    
    M_b(3, 3) = I_y_block - I_y_hole;
    M_b(4, 4) = I_z_block - I_z_hole;
    
    % Stiffness at each direction
    % K_b = 4x4
    % k_VV: assuming bar extension mode
    K_b = zeros(4);
    
    K_b(1, 1) =  2 * E_aco * E_L * B_L / L_L;
    % k_VV: assuming beam in bending mode
    I_b = B_L * E_L ^ 3 / 12;
    K_b(2, 2) = 2 * 12 * E_aco * I_b / L_L^3 * 0.93;
    % Ignoring gyroscopic forces in the bearings
    K_b(3, 3) = K_b(1, 1) * 1.47e-7;
    K_b(4, 4) = K_b(1, 1) * 1.0e-7;
    
    output_file = 'critical_frequencies.txt';
    
    % Damping of the bearings
    % Ignoring damping
    C_b = zeros(4);
    C_b(1, 1) = 1e-6;
    C_b(2, 2) = 1e-6;
    C_b(3, 3) = 1e-6;
    C_b(4, 4) = 1e-6;
    
    %% Define the shaft's main stations
    st_1 = 0;
    st_2 = L_12;
    st_3 = st_2 + L_23;
    st_4 = st_3 + L_34;

    model.num_stations = 4;
    model.stations = [st_1, st_2, st_3, st_4];
    clear st_1 st_2 st_3 st_4
    
    %% Define the materials
    
    % Material 1: steel
    material = cell(2, 1);
    mat_prop.E = E_aco;
    mat_prop.rho = rho_aco;
    material{1} = mat_prop;
    clear mat_prop
    
    % Material 2: aluminum
    mat_prop.E = E_al;
    mat_prop.rho = rho_al;
    material{2} = mat_prop;
    clear mat_prop
    
    model.num_materials = 2;
    model.material = material;
    clear material
    
    %% Define the volume geometries
    geometry_data = cell(4, 1);
    
    % First shaft section
    geo.r_o = D_e / 2;
    geo.r_i = 0;
    geo.L = L_12;
    geometry_data{1} = geo;
    clear geo
    
    % Second shaft section
    geo.r_o = D_e / 2;
    geo.r_i = 0;
    geo.L = L_23;
    geometry_data{2} = geo;
    clear geo
    
    % Third shaft section
    geo.r_o = D_e / 2;
    geo.r_i = 0;
    geo.L = L_34;
    geometry_data{3} = geo;
    clear geo
    
    % Disk
    geo.r = R_D;
    geo.t = E_D;
    geometry_data{4} = geo;
    clear geo
    
    model.num_geometries = 4;
    model.geometry_data = geometry_data;
    clear geometry_data
    
    %% Define the shaft's volumes
    volumes = cell(3, 1);
    
    % First shaft section
    vol.material = 1;
    vol.geometry = 1;
    vol.stations = [1, 2];
    vol.element_type = 'shaft2';
    vol.element_size = 10e-3;
    vol.element_num_min = 2;
    vol.element_num_max = 100;
    volumes{1} = vol;
    clear vol
    
    % Second shaft section
    vol.material = 1;
    vol.geometry = 2;
    vol.stations = [2, 3];
    vol.element_type = 'shaft2';
    vol.element_size = 10e-3;
    vol.element_num_min = 2;
    vol.element_num_max = 100;
    volumes{2} = vol;
    clear vol
    
    % Third shaft section
    vol.material = 1;
    vol.geometry = 3;
    vol.stations = [3, 4];
    vol.element_type = 'shaft2';
    vol.element_size = 10e-3;
    vol.element_num_min = 2;
    vol.element_num_max = 100;
    volumes{3} = vol;
    clear vol
    
    model.num_volumes = 3;
    model.volumes = volumes;
    clear volumes
    
    %% Add disks
    
    disks = cell(2, 1);
    
    % Left disk
    prop.m = m_d;
    prop.I_p = I_p;
    prop.I_d = I_d;
    prop.stations = 2;
    disks{1} = prop;
    clear prop
    
    % Right disk
    prop.m = m_d;
    prop.I_p = I_p;
    prop.I_d = I_d;
    prop.stations = 3;
    disks{2} = prop;
    clear prop
    
    model.num_disks = 2;
    model.disks = disks;
    clear disks
    
    %% Add the bearings
    
    bearings = cell(2, 1);
    
    % Left bearing
    bear.M = M_b;
    bear.K = K_b;
    bear.C = C_b;
    bear.stations = 1;
    bear.constrained_gdls = [5, 6];
    bearings{1} = bear;
    clear bear
    
    % Right bearing
    bear.M = M_b;
    bear.K = K_b;
    bear.C = C_b;
    bear.stations = 4;
    bear.constrained_DOFs = [5, 6];
    bearings{2} = bear;
    clear bear
    
    model.num_bearings = 2;
    model.bearings = bearings;
    clear bearings
    
    % Clear unnecessary parameters
    clear B_L B_m D_e D_m E_D E_L E_aco E_al E_m L_12 L_23 L_34 L_L L_m ...
        R_D c_b_cor k_b_cor m_b_cor rho_aco rho_al C_b I_b I_d I_p K_b ...
        M_b m_d
    
    %% Generate the mesh
    model.node_DOFs = 4;
    model = generate_mesh(model);
    
    %% Assemble the global inertia matrix in the fixed frame coordinates
    M_F = assemble_matrix_inertia(model);
    
    %% Assemble the global gyroscopic matrix in the fixed frame coordinates
    G_F = assemble_matrix_gyroscopic(model);
    
    %% Assemble the global damping matrix in the fixed frame coordinates
    C_F = assemble_matrix_damping(model);
    
    %% Assemble the global stiffness matrix in the fixed frame coordinates
    K_F = assemble_matrix_stiffness(model);
    
    %% Vector of loads, in the rotating frame, angular velocity Omega
    num_DOFs = model.mesh.num_nodes*model.node_DOFs;
    F_R = zeros(num_DOFs, 1);
    % Insert a perturbation at the first disk on the W direction
    disk_station = model.disks{1}.stations;
    disk_node = model.mesh.station_nodes(disk_station);
    pert_DOF = (disk_node - 1)*model.node_DOFs + 2;
    F_R(pert_DOF) = 1;
    
    %% Apply the boundary conditions
    % Node, DOF, value
    % Include in ascending order of nodes, and for each node in ascending
    % order of DOF.
%     first_node = 1;
%     last_node = model.mesh.num_nodes;
%     bcs = [ first_node, 3, 0;
%             first_node, 4, 0;
%              last_node, 3, 0;
%              last_node, 4, 0];
%     model.num_bcs = 4;
%     model.bcs = bcs;
%     clear bcs
%     
%     [M_F, G_F, ~, K_F, F_F] = apply_boundary_conditions(model, M_F, G_F, [], K_F, F_F);
    
    %% Convert to the state space formulation
    Omega = 50;
    
    A = sparse([zeros(size(K_F)), eye(size(K_F)); K_F, -2*pi*Omega*G_F]);
    B = sparse([-eye(size(K_F)), zeros(size(K_F)); zeros(size(K_F)), M_F]);
    [V, omega_c] = eigs(A, -1i*B, 20, 0);
    
    %% Calculate the natural frequencies
    K_s = sparse(K_F);
    M_s = sparse(M_F);
    [V0, omega_n_2] = eigs(K_s, M_s, 10, 0);
    % eliminate residual terms
    V0(abs(V0) < 1e-6) = 0;
    % Find natural frequencies at Omega = 0 Hz
    omega_n = sqrt(diag(omega_n_2));
    [omega_n, V0_ind] = sort(omega_n);
    V0 = V0(:, V0_ind);
    
    dis_num_DOFs = model.mesh.num_nodes;
    
    % Separate the W and V DOFs from the others
    ind_y = zeros(1, dis_num_DOFs);
    ind_z = zeros(1, dis_num_DOFs);
    ind_y(1) = 1;
    ind_z(1) = 2;
    for j = 2:dis_num_DOFs
        ind_y(j) = ind_y(j-1) + 4;
        ind_z(j) = ind_z(j-1) + 4;
    end
    
    fprintf('Natural frequencies, Hz, at Omega = 0 Hz\n')
    freq_n = omega_n / (2 * pi);
    disp(freq_n(1:10))
    
    fprintf('Damped natural frequencies, Hz , at Omega = %g Hz(state-space)\n', Omega)
    freq_c = abs(diag(omega_c)) / (2 * pi);
    [freq_c, V_ind] = sort(freq_c);
    freq_c = freq_c(1:2:20);
    disp(freq_c)
    V = V((1:num_DOFs) + num_DOFs, V_ind);
    V = V(:, 1:2:20);
    V_y = V(ind_y, :);
    V_z = V(ind_z, :);
    
    fig_mode = figure('OuterPosition', [0, 50, 500, 500]);
    set(fig_mode, 'PaperUnits', 'centimeters');
    set(fig_mode, 'PaperSize', [16 16]);
    set(fig_mode, 'PaperPositionMode', 'manual');
    set(fig_mode, 'PaperPosition', [0 0 16 16]);
    ax_mode_V = subplot(2, 1, 1);
    set(ax_mode_V, 'XLim', [1 model.mesh.num_nodes], 'XTick', 1:4:model.mesh.num_nodes)
    set(ax_mode_V, 'YLim', [-1.5, 1.5], 'YTick', -1:1)
    title('Mode j, \omega_n = X Hz, \Omega = 0 Hz')
    ylabel('V')
    set(ax_mode_V, 'LooseInset', get(ax_mode_V, 'TightInset'));
    ax_mode_W = subplot(2, 1, 2);
    set(ax_mode_W, 'XLim', [1 model.mesh.num_nodes], 'XTick', 1:4:model.mesh.num_nodes)
    set(ax_mode_W, 'YLim', [-1.5, 1.5], 'YTick', -1:1)
    xlabel('Node number')
    ylabel('W')
    set(ax_mode_W, 'LooseInset', get(ax_mode_W, 'TightInset'));
    
    fid_omega = fopen(output_file, 'w', 'l', 'UTF-8');
    fprintf(fid_omega, '# mode      omega_c        Omega\n');
    for mode_j = 1:10
        % Plot the null rotation modes
        % At Y direction
        cla(ax_mode_V)
        set(ax_mode_V, 'NextPlot', 'add')
        plot(ax_mode_V, 1:model.mesh.num_nodes, V0(ind_y, mode_j), ...
            'Color', [.7 .7 0], 'LineWidth', 1);
        plot(ax_mode_V, [1, model.mesh.num_nodes], [0, 0], '-.k');
        set(ax_mode_V, 'NextPlot', 'replaceChildren')
        
        fprintf(fid_omega, '% 6d %12.6e %12.6e\n', mode_j, freq_n(mode_j), 0);
        desc = sprintf('Mode %d', mode_j);
        title(ax_mode_V, desc)
        
        % At Z direction
        cla(ax_mode_W)
        set(ax_mode_W, 'NextPlot', 'add')
        plot(ax_mode_W, 1:model.mesh.num_nodes, V0(ind_z, mode_j), ...
            'Color', [.7 .7 0], 'LineWidth', 1);
        plot(ax_mode_W, [1, model.mesh.num_nodes], [0, 0], '-.k');
        set(ax_mode_W, 'NextPlot', 'replaceChildren')
        filename = sprintf('Mode_%03d', mode_j);
        plot_flags = [0, 1, 2, 3];
        save_figure(fig_mode, filename, model.fig_dir, plot_flags);
    end
    fprintf(fid_omega, '\n');
    
    fig_comp = figure('OuterPosition', [500, 50, 500, 500]);
    set(fig_comp, 'PaperUnits', 'centimeters');
    set(fig_comp, 'PaperSize', [16 16]);
    set(fig_comp, 'PaperPositionMode', 'manual');
    set(fig_comp, 'PaperPosition', [0 0 16 16]);
    ax_comp = axes;
    set(ax_comp, 'XLim', [1 model.mesh.num_nodes], 'XTick', 1:4:model.mesh.num_nodes)
    set(ax_comp, 'YLim', [-1 1], 'YTick', -1:.5:1)
    set(ax_comp, 'ZLim', [-1 1], 'ZTick', -1:.5:1)
    view(ax_comp, -80, 20)
    xlabel('Node number')
    ylabel('real')
    zlabel('imag')
    title('Complex mode j - \omega_r = X - \Omega = X')
    set(ax_comp, 'LooseInset', get(ax_comp, 'TightInset'));
    fprintf(fid_omega, '# mode      omega_c        Omega\n');
    for mode_j = 1:10
        
        % Plot the complex rotation modes
        V_y_j = V_y(:, mode_j);
        V_z_j = V_z(:, mode_j);
        % Normalize by the greatest amplitude
        amp = max(abs([V_y_j; V_z_j]));
        V_y_j = V_y_j / amp;
        V_z_j = V_z_j / amp;
        
        cla(ax_comp)
        set(ax_comp, 'NextPlot', 'add')
        fill3([1, 1:model.mesh.num_nodes, model.mesh.num_nodes], ...
            [0, real(V_y_j)', 0], [0, imag(V_y_j)', 0], [0, abs(V_y_j)', 0])
        
        fill3([1, 1:model.mesh.num_nodes, model.mesh.num_nodes], ...
            [0, real(V_z_j)', 0], [0, imag(V_z_j)', 0], [0, abs(V_z_j)', 0]+1)
        
        plot3(ax_comp, [1, model.mesh.num_nodes], [0, 0], [0, 0], '-.r', 'LineWidth', 1)
        alpha 0.7
        set(ax_comp, 'NextPlot', 'replaceChildren')
        
        colormap([(1 - bone(128)); (1-pink(128))]*.8)
        caxis([0, 2]);
        
        fprintf(fid_omega, '% 6d %12.6e %12.6e\n', mode_j, freq_n(mode_j), Omega);
        desc = sprintf('Complex mode %d', mode_j);
        title(ax_comp, desc)
        
        filename = sprintf('Mode_complex_%03d', mode_j);
        plot_flags = [0, 3, 4];
        save_figure(fig_comp, filename, model.fig_dir, plot_flags);
    end
    fclose(fid_omega);
    
    %% Plot the Campbell's Chart
    % From 0 up to 200 Hz
    freq_vec = 0:1:250;
    campbell_chart(model, M_F, G_F, [], K_F, freq_vec, 10);
    
    %% Plot the directional FRFs
    freq_vec = linspace(0, 100, 1000);
    plot_FRF(model, M_F, G_F, C_F, K_F, [0 1], disk_node, freq_vec);
    plot_FRF_rot(model, M_F, G_F, C_F, K_F, [0 1 0 0], [-1 0 0 0], disk_node, freq_vec);
    plot_FRFd(model, M_F, G_F, C_F, K_F, F_R, freq_vec);
end
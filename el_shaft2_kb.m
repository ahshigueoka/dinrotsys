function K_B_e = el_shaft2_kb(model, vol_idx)
% array = el_shaft2_kb(model m, int vol_idx)
%
% Creates the elementar bending matrix from the properties
% defined in the volume identified by "vol_idx" and the data given in the
% model structure "m".
%
    geo_idx = model.volumes{vol_idx}.geometry;
    mat_idx = model.volumes{vol_idx}.material;
    
    r_o = model.geometry_data{geo_idx}.r_o;
    r_i = model.geometry_data{geo_idx}.r_i;
    I = pi * (r_o^4 - r_i^4) / 4;
    
    L = model.geometry_data{geo_idx}.L / model.volumes{vol_idx}.num_int_elements;
    
    E = model.material{mat_idx}.E;
    
    K_B_e = [   12        0        0        6*L    -12        0        0        6*L   ;...
                 0       12       -6*L      0        0      -12       -6*L      0     ;...
                 0       -6*L      4*L^2    0        0        6*L      2*L^2    0     ;...
                 6*L      0        0        4*L^2   -6*L      0        0        2*L^2 ;...
               -12        0        0       -6*L     12        0        0       -6*L   ;...
                 0      -12        6*L      0        0       12        6*L      0     ;...
                 0       -6*L      2*L^2    0        0        6*L      4*L^2    0     ;...
                 6*L      0        0        2*L^2   -6*L      0        0        4*L^2];
   K_B_e = E * I / L^3 * K_B_e;
end